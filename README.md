# BigBlueButton Ansible Role

[![pipeline status](https://git.coop/meet/bbb/badges/master/pipeline.svg)](https://git.coop/meet/bbb/-/commits/master)

The [tasks/main.yml](tasks/main.yml) steps follow [the BigBlueButton 2.2
Install page](https://docs.bigbluebutton.org/2.2/install.html).

The [tasks/greenlight.yml](tasks/greenlight.yml) steps follow [the Greenlight
Install page](https://docs.bigbluebutton.org/greenlight/gl-install.html).

The role will need refactoring when there is a version other than 2.2 on Xenial
to support.

## Automated Testing

We're using [Molecule](https://molecule.readthedocs.io/en/latest/).

The build relies on our own purpose built [container registry](https://git.coop/meet/containers/container_registry) images.

On each merge request or pushed commit, the role will be tested. See the [pipelines](https://git.coop/meet/bbb/pipelines) for logs.

If you need a faster feedback loop and want to run things on your local, here is how you do it.

You'll need to have [Docker](https://www.docker.com/) installed. If you don't already have it, you can do the following.

```bash
$ curl -fsSL https://get.docker.com -o get-docker.sh
$ sh get-docker.sh   # have a look first if you want
```

Please note the following while running test containers on your local:

- We do use **privileged: true** flag for Docker containers
- We pass in **/sys/fs/cgroup** and **/var/run/docker.sock** as read-only volume mounts

Now, install Molecule and dependencies and run the tests.

```bash
$ python3 -m venv .venv && source .venv/bin/activate
$ pip install 'molecule[docker]==3.0.4'
$ molecule test
```

Use the **--destroy=never** flag to not destroy the container so you can `molecule login` to manually debug.

If you're really stuck with an issue, export some env vars to tell Molecule to output more logs.

```bash
$ export MOLECULE_NO_LOG=False  # see more logs
$ export MOLECULE_DEBUG=True  # very verbose logs
```

You can run each individual Molecule step (`molecule test` runs them all). See `molecule --help` for more.

## Greenlight

Admin accounts can be added via dictionaries of variables like this:

```yml
bbb_admins:
  chris:
    email: chris@webarchitects.co.uk
```

This will trigger a email to be sent from the server to the email address,
containing the password, you need to have PTR records and ideally DKIM in place
before this will be reliable.

There is no Ansible based account removal, that needs to be done using the
Greenligh web interface.

### Greenlight CLI

Enter the container:

```bash
cd /var/greenlight
docker exec -it greenlight-v2 /bin/bash
bundle exec rake --tasks
rake about                                           # List versions of all Rails frameworks and the environment
rake active_storage:install                          # Copy over the migration needed to the application
rake admin:create[name,email,password,role]          # Creates an administrator account
rake app:template                                    # Applies the template supplied by LOCATION=(/path/to/template) or URL
rake app:update                                      # Update configs and some other initially generated files (or use just update:configs or update:bin)
rake assets:clean[keep]                              # Remove old compiled assets
rake assets:clobber                                  # Remove compiled assets
rake assets:environment                              # Load asset compile environment
rake assets:precompile                               # Compile all the assets named in config.assets.precompile
rake autoprefixer:info                               # Show selected browsers and prefixed CSS properties and values
rake cache_digests:dependencies                      # Lookup first-level dependencies for TEMPLATE (like messages/show or comments/_comment.html)
rake cache_digests:nested_dependencies               # Lookup nested dependencies for TEMPLATE (like messages/show or comments/_comment.html)
rake conf:check                                      # Check Configuration
rake db:create                                       # Creates the database from DATABASE_URL or config/database.yml for the current RAILS_ENV (use db:create:al...
rake db:drop                                         # Drops the database from DATABASE_URL or config/database.yml for the current RAILS_ENV (use db:drop:all to...
rake db:environment:set                              # Set the environment value for the database
rake db:fixtures:load                                # Loads fixtures into the current environment's database
rake db:migrate                                      # Migrate the database (options: VERSION=x, VERBOSE=false, SCOPE=blog)
rake db:migrate:status                               # Display status of migrations
rake db:rollback                                     # Rolls the schema back to the previous version (specify steps w/ STEP=n)
rake db:schema:cache:clear                           # Clears a db/schema_cache.yml file
rake db:schema:cache:dump                            # Creates a db/schema_cache.yml file
rake db:schema:dump                                  # Creates a db/schema.rb file that is portable against any DB supported by Active Record
rake db:schema:load                                  # Loads a schema.rb file into the database
rake db:seed                                         # Loads the seed data from db/seeds.rb
rake db:setup                                        # Creates the database, loads the schema, and initializes with the seed data (use db:reset to also drop the...
rake db:structure:dump                               # Dumps the database structure to db/structure.sql
rake db:structure:load                               # Recreates the databases from the structure.sql file
rake db:version                                      # Retrieves the current schema version number
rake dev:cache                                       # Toggle development mode caching on/off
rake initializers                                    # Print out all defined initializers in the order they are invoked by Rails
rake log:clear                                       # Truncates all/specified *.log files in log/ to zero bytes (specify which logs with LOGS=test,development)
rake middleware                                      # Prints out your Rack middleware stack
rake notes                                           # Enumerate all annotations (use notes:optimize, :fixme, :todo for focus)
rake notes:custom                                    # Enumerate a custom annotation, specify with ANNOTATION=CUSTOM
rake office365:migrate                               # Migrates over old office365 users to new account
rake restart                                         # Restart app by touching tmp/restart.txt
rake room:remove[include_used]                       # Removes all rooms for users that can't create rooms
rake routes                                          # Print out all defined routes in match order, with names
rake secret                                          # Generate a cryptographically secure secret key (this is typically used to generate a secret for cookie se...
rake stats                                           # Report code statistics (KLOCs, etc) from the application or engine
rake test                                            # Run tests / Runs all tests in test folder except system ones
rake test:db                                         # Run tests quickly, but also reset db
rake test:system                                     # Run system tests only
rake time:zones[country_or_offset]                   # List all time zones, list by two-letter country code (`rails time:zones[US]`), or list by UTC offset (`ra...
rake tmp:clear                                       # Clear cache, socket and screenshot files from tmp/ (narrow w/ tmp:cache:clear, tmp:sockets:clear, tmp:scr...
rake tmp:create                                      # Creates tmp directories for cache, sockets, and pids
rake user:create[name,email,password,role,provider]  # Creates a user account
rake yarn:install                                    # Install all JavaScript dependencies as specified via Yarn
```
